module.exports = {
  email: {
    isEmail: {
      errorMessage: 'Must be a valid email.',
      options: {
        domain_specific_validation: true
      }
    },
    in: ['body']
  },
  password: {
    isLength: {
      errorMessage: 'Password should be at least 8 characters long.',
      options: {
        min: 8
      }
    },
    isDecimal: {
      errorMessage: 'Expecting numbers only.'
    },
    in: ['body']
  }
}
