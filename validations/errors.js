module.exports = (errors) => {
  const processedErrors = {}
  errors.forEach(error => {
    if (!processedErrors[error.param]) processedErrors[error.param] = []
    processedErrors[error.param].push(error)
  })
  return processedErrors
}
