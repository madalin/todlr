const { User } = require('../models')
const { passport } = require('../providers')
const { hash } = require('bcrypt')
const { validationResult } = require('express-validator')
const processErrors = require('../validations/errors')

module.exports.register = async (req, res) => {
  const errors = validationResult(req)
  console.log(processErrors(errors.array()))
  if (!errors.isEmpty()) {
    return res.render(
      'auth/register',
      {
        csrfToken: req.csrfToken(),
        errors: errors.array()
      }
    )
  }
  try {
    const hashedPassword = await hash(req.body.password, 10)
    const user = User.build({
      email: req.body.email.trim(),
      password: hashedPassword
    })
    await user.save()
    res.redirect('/')
  } catch (error) {
    console.log(error.message)
    // @TODO implement express-flash
    res.redirect('back')
  }
}

module.exports.login = () => {
  return passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: 'back',
    failureFlash: false
  })
}

module.exports.logout = (req, res) => {
  req.logout()
  res.redirect('/')
}

module.exports.reset = async (req, res) => {
  // check email
  // check if is a valid email
  // check if email is already in the db
  if (!req.body.email) {
    console.log('Email is required!')
    res.redirect(301, 'back')
  }

  try {
    const user = await User.findOne({ where: { email: req.body.email.trim() } })
    if (user !== null) {
      console.log('I would send a password reset for ' + user.email)
      // @TODO implement password reset by email
    }
  } catch (error) {
    console.log(error.message)
  }
  res.redirect('back')
}
