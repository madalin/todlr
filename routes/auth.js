var express = require('express')
var router = express.Router()
var { checkSchema } = require('express-validator')
const validation = require('../validations/schemas')
var { login, logout, register, reset } = require('../controllers/auth')

router.post('/reset', reset)

router.post('/logout', logout)

router.post(
  '/register',
  checkSchema(validation.registration),
  register
)

router.post('/login', login())

module.exports = router
