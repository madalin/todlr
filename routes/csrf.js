var express = require('express')
var router = express.Router()

router.get('/', (req, res) => {
  res.cookie('XSRF-TOKEN', req.csrfToken())
  res.send(204)
})

module.exports = router
