const { User } = require('../models')
// const { hash } = require('bcrypt')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy

passport.serializeUser((user, done) => {
  done(null, user)
})

passport.deserializeUser(async (user, done) => {
  done(null, user)
})

passport.use(
  new LocalStrategy({
    usernameField: 'email'
  }, async (username, password, done) => {
    const user = await User.findOne({ where: { email: username } })
    if (user === null) {
      return done(null, false, { message: 'Incorrect username.' })
    }
    return done(null, user)
  })
)

module.exports = passport
