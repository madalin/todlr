const createError = require('http-errors') // @TODO Research
const express = require('express')
const logger = require('morgan')
const redis = require('redis')
const session = require('express-session')
const csrf = require('csurf')
const passport = require('passport')

const RedisStore = require('connect-redis')(session)
const redisClient = redis.createClient()

const csrfRouter = require('./routes/csrf')
const authRouter = require('./routes/auth')

const app = express()

/**
 * INIT SESSION
 *
 * @TODO Use ENV
 */
app.use(session({
  store: new RedisStore({ client: redisClient }),
  cookie: {
    path: '/',
    httpOnly: true,
    secure: false,
    maxAge: null
  },
  name: 'sessionId',
  resave: false,
  // GDPR
  saveUninitialized: false,
  secret: 'changemetoenv'
}))

app.use(passport.initialize())
app.use(passport.session())

/**
 * Set login state
 */
app.use((req, res, next) => {
  res.locals.login = req.isAuthenticated()
  next()
})

/**
 * INIT LOGS
 */
app.use(logger('dev'))

/**
 * INIT REQ DATA PREPROCESS
 */
app.use(express.json())

/**
 * INIT ROUTES
 * Mixed with just before middlewares where needed.
 *
 * @TODO Decide if extra middlewares should be separated to relevant route file
 */
// CSRF is checked only after body has been parsed!
app.use(csrf())
app.use('/csrf', csrfRouter)
app.use('/auth', authRouter)

/**
 * 404
 */
app.use((req, res, next) => {
  next(createError(404))
})

/**
 * ERROR HANDLER
 */
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  // @TODO create dedicated view for error pages
  res.render('error')
})

/**
 * That's all folks!
 */
module.exports = app
